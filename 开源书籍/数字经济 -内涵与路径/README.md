# 1. 《数字经济：内涵与路径概述》

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84.png)

国家《“十四五”数字经济发展规划》指出：“数字经济是继农业经济、工业经济之后的主要经济形态”，是以数据资源为关键要素，以现代信息网络为主要载体，以信息通信技术融合应用、全要素数字化转型为重要推动力，促进公平与效率更加统一的新经济形态。“

《数字经济：内涵与路径概述》描述的理论框架包括数字生产力、数字生产关系、数据要素（数据要素化和要素数据化）、数字应用（产业互联网、数字金融）、数字治理等方面。这一框架体系把技术、市场、治理等几个视觉统一在一起，建立了“围绕如何激活数据要素，发展数字生产力，建立数字化生产关系”的数字经济发展主线，从政治经济学的视角，描述了一个原创的数字经济理论体系。

通过对传统生产力的劳动对象、劳动资料和劳动者，作者给出了数字时代对应的理解，提出算法（劳动工具）、连接信息（劳动对象）、分析师（劳动者）是数字生产力的表现形式，并在这一逻辑下分析了劳动对象的“五全”特性，指出“五全”信息是数字生产力巨大创造力的基础。针对生产关系中的生产资料所有制，人们在生产中的地位和交换关系、产品的分配方式，书中分别探讨了数字化所带来的转变，并重点分析了在区块链、云计算等技术支持下，网状对人际关系的形态和价值，提出了“智慧人口红利”这一重要概念。此外，面对数字经济的热点问题，有针对性地构建了平台经济、产业互联网、产业数字金融和数字治理的知识架构，并创造性提出了一系列解决方案。

## 关于作者

《数字经济：内涵与路径》的第一作者是黄奇帆教书，他既是中国知名的经济学家、复旦大学的客座教授，也曾经是主政一方的官员。黄奇帆教授联合清华大学朱岩教授和平安银行原行长邵平先生，对中国数字经济的内涵和发展路径进行了分析和总结，共同推出了这本语言平实、内涵丰富的著作。

# 2. 目录

## [第一章 数字经济概述](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/01-%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%E6%A6%82%E8%BF%B0.md)
## [第二章 数字逻辑：数字经济与东方哲学的一致性](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/02-%E6%95%B0%E5%AD%97%E9%80%BB%E8%BE%91%20-%20%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%E4%B8%8E%E4%B8%9C%E6%96%B9%E5%93%B2%E5%AD%A6%E7%9A%84%E4%B8%80%E8%87%B4%E6%80%A7.md)
## [第三章 数字生产力](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/03-%E6%95%B0%E5%AD%97%E7%94%9F%E4%BA%A7%E5%8A%9B.md)
## [第四章 数字生产关系](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/04-%E6%95%B0%E5%AD%97%E7%94%9F%E4%BA%A7%E5%85%B3%E7%B3%BB.md)
## [第五章 数据要素化与要素数据化](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/05-%E6%95%B0%E6%8D%AE%E8%A6%81%E7%B4%A0%E5%8C%96%E4%B8%8E%E8%A6%81%E7%B4%A0%E6%95%B0%E6%8D%AE%E5%8C%96.md)
## [第六章 数字经济的平台化发展：从消费互联网到产业互联网](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/06-%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%E7%9A%84%E5%B9%B3%E5%8F%B0%E5%8C%96%E5%8F%91%E5%B1%95.md)
## [第七章 数字金融](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/07-%E6%95%B0%E5%AD%97%E9%87%91%E8%9E%8D.md)
## [第八章 数字治理](https://gitee.com/openatom-university/osstheory-fundamental/blob/develop/%E5%BC%80%E6%BA%90%E4%B9%A6%E7%B1%8D/%E6%95%B0%E5%AD%97%E7%BB%8F%E6%B5%8E%20-%E5%86%85%E6%B6%B5%E4%B8%8E%E8%B7%AF%E5%BE%84/08-%E6%95%B0%E5%AD%97%E6%B2%BB%E7%90%86.md)