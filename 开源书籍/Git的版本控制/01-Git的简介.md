简单地说，Git是一个内容跟踪器。鉴于这一概念，Git与大多数版本控制系统具有相同的原则。然而，使Git在当今各种可用工具中独具一格的独特特性是它是一个分布式版本控制系统。这种区别意味着Git是快速和可伸缩的，拥有丰富的命令集集合，提供对高级和低级操作的访问，并且针对本地操作进行了优化。

在本章中，您将学习Git的基本原理、特性和基本Git命令，并将获得一些关于创建和向存储库添加更改的快速指导。

我们强烈建议您花点时间来掌握这里解释的重要概念。这些主题是Git的构建块，将帮助您理解管理Git存储库的中级和高级技术，并将其作为日常工作的一部分。当我们在第二部分“Git基础知识”、第三部分“中级技能”和第四部分“高级技能”中分解Git的内部工作原理时，这些基本概念也将帮助你加速学习。

# 1. Git组件

在深入了解git命令之前，让我们先后退一步，对组成git生态系统的组件进行可视化概述。

各组件的工作原理如图1-1所示。

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Version%20Control%20with%20Git/Version%20Control%20with%20Git-001.png)

GitGUI工具充当Git命令行的前端，一些工具具有与流行的Git托管平台集成的扩展。Git客户端工具主要在存储库的本地副本上工作。

在使用Git时，典型的设置包括Git服务器和Git客户端。你可以不需要一个服务器，但是当你在协作设置中共享修订变更时，这会增加你维护和管理存储库的复杂性，并且会使一致性变得更加困难(我们将在第11章中重新讨论这个问题)。Git服务器和客户端工作如下：

- Git服务器：Git服务器使您能够更轻松地进行协作，因为它确保了您将要处理的存储库的中心可靠的真实来源的可用性。Git服务器也是存储远程Git存储库的地方;按照惯例，存储库拥有项目的最新和最稳定的源代码。您可以选择安装和配置自己的Git服务器，也可以选择将Git存储库托管在可靠的第三方托管站点(如GitHub、GitLab和Bitbucket)上。

- Git客户端：Git客户端与本地存储库交互，您可以通过Git命令行或Git GUI工具与Git客户端交互。在安装和配置Git客户端时，您将能够访问远程存储库，在存储库的本地副本上工作，并将更改推回Git服务器。如果你是Git的新手，我们建议你从Git命令行开始;熟悉日常操作所需的常见git命令子集，然后再学习您选择的Git GUI工具。

采用这种方法的原因是，在某种程度上，Git GUI工具倾向于提供表示期望结果的术语，而这些结果可能不是Git标准命令的一部分。一个例子是一个带有sync选项的工具，它掩盖了两个或多个git命令的底层链接，以实现预期的结果。如果出于某种原因，你要在命令行上输入sync子命令，你可能会得到以下令人困惑的输出：

~~~
$ git sync
git: 'sync' is not a git command. See 'git --help'.
The most similar command is svn
~~~

> 注意：Git sync不是一个有效的Git子命令。为了确保存储库的本地工作副本与来自远程Git存储库的更改同步，您将需要运行以下命令的组合:Git fetch、Git merge、Git pull或Git push。

有大量的工具可供您使用。一些Git GUI工具非常漂亮，可以通过插件模型进行扩展，可以选择连接和利用流行的第三方Git托管站点上提供的功能。虽然通过GUI工具学习Git很方便，但我们将重点关注Git命令行工具的示例和代码讨论，

因为这建立了一个良好的基础知识，将导致Git的灵活性。

# 2. Git的特征

现在我们已经概述了Git组件，下面让我们来了解一下Git的特征。了解Git的这些独特特征，您就可以轻松地从集中式版本控制思想切换到分布式版本控制思想。我们喜欢将其称为“在Git中思考”：

**Git将修订更改存储为快照**

首先要忘记的概念是Git存储您正在处理的文件的多个修订的方式。与其他版本控制系统不同，Git不会将修订更改作为一系列修改(通常称为增量)进行跟踪;相反，它获取在特定时间点对存储库状态所做更改的快照。在Git术语中，这被称为提交。把这想象成通过一张照片捕捉一个瞬间。

**Git针对本地开发进行了增强**

在Git中，您可以在本地开发机器上处理存储库的副本。这被称为本地存储库，或Git服务器上远程存储库的克隆。您的本地存储库将在一个位置拥有对这些资源所做的修订更改的资源和快照。Git将这些链接快照集合称为存储库提交历史，或简称为存储库历史。这允许您在一个断开连接的环境中工作，因为Git不需要一直连接到Git服务器来版本控制您的更改。自然的结果是，您能够跨分布式团队处理大型、复杂的项目，而不会影响版本控制操作的效率和性能。

**Git是显式的**

Git命令是显式的，Git需要等待你提供做什么以及何时做的说明。例如，Git不会自动将更改从本地存储库同步到远程存储库，也不会自动将修订的快照保存到本地存储库的历史记录。每个操作都需要你明确的命令或指令来告诉Git需要什么，包括添加新提交、修复现有提交、将更改从本地存储库推送到远程存储库，甚至从远程存储库检索新更改。简而言之，你需要有意识地采取行动。这还包括让Git知道你要跟踪哪些文件，因为Git不会自动添加新文件进行版本控制。

**Git旨在支持非线性开发**

Git允许您构思和试验项目可行解决方案的各种功能实现，允许您沿着项目的主要、稳定的代码库并行工作。

这种方法称为分支，是一种非常常见的实践，它可以确保主线开发的完整性，防止任何可能破坏主线的意外更改。

在Git中，分支的概念被认为是轻量级和廉价的，因为Git中的分支只是指向一系列链接提交中的最新提交的指针。对于您创建的每个分支，Git都会跟踪该分支的一系列提交。您可以在本地分支之间切换。然后，Git将项目状态恢复到创建指定分支快照时的最近时刻。当你决定将任何分支的更改合并到主开发线中时，Git能够通过应用我们将在第6章中讨论的技术来合并这一系列提交。

> 提示:由于Git提供了许多新功能，请记住，其他版本控制系统的概念和实践在Git中可能工作方式不同，或者根本不适用。

# 3. Git的命令行

Git的命令行界面使用简单。它的设计目的是将存储库的完全控制权交给您。因此，做同样的事情有很多方法。通过关注哪些命令对您的日常工作很重要，我们可以简化并更深入地学习它们。

作为起点，只需输入git version或git --version来确定你的机器是否已经预装了git。您应该会看到类似如下的输出：

~~~
$ git --version
git version 2.30.2
~~~

如果您的电脑上没有安装Git，请参考附录B了解如何根据您的操作系统平台安装Git，然后再继续下一节。

在安装时，输入不带任何参数的git。Git会列出它的选项和最常见的子命令：

~~~
~$ git
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone             Clone a repository into a new directory
   init              Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add               Add file contents to the index
   mv                Move or rename a file, a directory, or a symlink
   restore           Restore working tree files
   rm                Remove files from the working tree and from the index
   sparse-checkout   Initialize and modify the sparse-checkout

examine the history and state (see also: git help revisions)
   bisect            Use binary search to find the commit that introduced a bug
   diff              Show changes between commits, commit and working tree, etc
   grep              Print lines matching a pattern
   log               Show commit logs
   show              Show various types of objects
   status            Show the working tree status

grow, mark and tweak your common history
   branch            List, create, or delete branches
   commit            Record changes to the repository
   merge             Join two or more development histories together
   rebase            Reapply commits on top of another base tip
   reset             Reset current HEAD to the specified state
   switch            Switch branches
   tag               Create, list, delete or verify a tag object signed with GPG

collaborate (see also: git help workflows)
   fetch             Download objects and refs from another repository
   pull              Fetch from and integrate with another repository or a local branch
   push              Update remote refs along with associated objects

'git help -a' and 'git help -g' list available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.
See 'git help git' for an overview of the system.
~~~

> 提示：要查看git子命令的完整列表，请输入git help --all。

正如您可以从使用提示中看到的，一小部分选项适用于git。大多数选项，如提示中的[ARGS]所示，应用于特定的子命令。

例如，选项--version会影响git命令并生成一个版本号：

~~~
$ git --version
git version 2.30.2
~~~

相反，--amend是特定于git的选项的一个例子子命令提交:

~~~
$ git commit --amend
~~~

有些调用需要这两种形式的选项(在这里，命令行中额外的空格只是为了在视觉上将子命令与基本命令分开，而不是必需的)：

~~~
$ git --git-dir=project.git	repack -d
~~~

为方便起见，每个git子命令的文档都可以使用git help subcommand、git --help subcommand、git subcommand --help或man git -subcommand获取。

从历史上看，Git是作为许多简单、独特、独立命令的套件提供的，这些命令是根据Unix哲学开发的:构建小型、可互操作的工具。每个命令都有一个连字符的名字，比如git- commit和git-log。但是，现代Git安装不再支持连字符的命令形式，而是使用带有子命令的单个Git可执行文件。

git命令理解“短”和“长”选项。 例如，git commit命令等效地处理以下示例：

~~~
$ git commit -m "Fix a typo."
$ git commit --message="Fix a typo."
~~~

短形式-m使用一个连字符，而长形式——message使用两个连字符。(这与GNU long options扩展一致。)有些选项只以一种形式存在。

> 提示:你可以通过多次使用-m选项创建一个提交摘要和详细的摘要消息：$ git commit -m "Summary" -m "Detail of Summary"

最后，可以通过双破折号约定将选项从参数列表中分离出来。例如，使用双破折号将命令行的控制部分与操作数列表(如文件名)进行对比：

~~~
$ git diff -w main origin -- tools/Makefile
~~~

您可能需要使用双破折号来分隔和显式标识文件名，以便它们不会被误认为命令的另一部分。例如，如果你碰巧有一个名为main.c的文件和一个标记，那么你需要有意识地执行你的操作：

~~~
# Checkout the tag named "main.c"
$ git checkout main.c

# Checkout the file named "main.c"
$ git checkout -- main.c
~~~

# 3. Git使用的快速介绍

要查看Git的运行情况，您可以创建一个新的存储库、添加一些内容并跟踪一些修订。 您可以通过两种方式创建存储库：从头开始创建存储库并用一些内容填充它，或者通过从远程 Git 服务器克隆现有存储库来使用它。

## 准备使用Git

无论您是创建新的存储库还是使用现有的存储库，在本地开发机器上安装Git之后，都需要完成一些基本的必备配置。这类似于在拍摄第一张快照之前在新相机上设置正确的日期、时区和语言。

Git至少需要您的姓名和电子邮件地址，才能在存储库中进行第一次提交。然后，您提供的标识显示为提交作者，与其他快照元数据集成在一起。你可以使用git config命令将你的身份保存在一个配置文件中：

~~~
$ git config user.name "Jon Loeliger"
$ git config user.email "jdl@example.com"
~~~

如果你决定不在配置文件中包含你的身份，你必须为每个git commit子命令指定你的身份，在命令的末尾附加参数——author：

~~~
$ git commit -m "log message" --author="Jon Loeliger <jdl@example.com>"
~~~

请记住，这是一种困难的方式，很快就会变得乏味。

您还可以通过向GIT_AUTHOR_NAME和GIT_AUTHOR_EMAIL环境变量分别提供姓名和电子邮件地址来指定您的身份。如果设置了，这些变量将覆盖所有配置设置。

但是，对于在命令行上设置的规范，Git将覆盖配置文件和环境变量中提供的值。

## 使用本地存储库

既然已经配置了标识，就可以开始使用存储库了。首先在本地开发机器上创建一个新的空存储库。我们将从简单开始，逐步学习在Git服务器上使用共享存储库的技术。

### 创建初始存储库

我们将模拟一个典型的情况，为您的个人网站创建一个存储库。让我们假设您从头开始，您将在本地目录~/my_website中为项目添加内容，并将其放置在Git存储库中。

输入以下命令创建目录，并将一些基本内容放在一个名为index.xhtml的文件中：

~~~
$ mkdir ~/my_website
$ cd ~/my_website
$ echo 'My awesome website!' > index.xhtml
~~~

要将~/my_website转换为Git存储库，请运行Git init。这里我们提供了选项-b，后面跟着一个名为main的默认分支：

~~~
$ git init -b main
Initialized empty Git repository in ../my_website/.git/
~~~

如果你想先初始化一个空Git存储库，然后再向它添加文件，你可以通过运行以下命令来做到这一点：

~~~
$ git init -b main ~/my_website
Initialized empty Git repository in ../my_website/.git/
$ cd ~/my_website
$ echo 'My awesome website!' > index.xhtml
~~~

> 提示：您可以初始化一个完全空的目录或一个充满文件的现有目录。在这两种情况下，将目录转换为Git存储库的过程是相同的。

git init命令在项目的根级创建一个名为.git的隐藏目录。所有修订信息以及支持元数据和Git扩展都存储在这个顶级的、隐藏的. Git文件夹中。


Git认为~/my_website是工作目录。此目录包含您的网站文件的当前版本。当您更改现有文件或向项目添加新文件时，Git会将这些更改记录在隐藏的. Git文件夹中。

为了便于学习，我们将引用两个虚拟目录，分别称为Local History和Index，以说明初始化新的Git存储库的概念。我们将分别在第4章和第5章讨论本地历史和索引。

如图1-2所示：

└── my_website
├── .git/
│	└── Hidden git objects
└── index.xhtml

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Version%20Control%20with%20Git/Version%20Control%20with%20Git-002.png)

本地历史记录和索引周围的虚线表示.git文件夹中的隐藏目录。

### 向存储库中添加文件

到目前为止，您只创建了一个新的Git存储库。换句话说，这个Git存储库是空的。虽然文件index.xhtml存在于~/my_website目录中，但对于Git来说，这是工作目录，表示一个您经常更改文件的刮擦板或目录。

当你完成了对文件的修改，并想要将这些更改存入Git存储库时，你需要使用Git add file命令显式地这样做：

~~~
$ git add index.xhtml
~~~

> 警告：尽管您可以让Git使用git add命令添加目录和所有子目录中的所有文件。 这会暂存所有内容，我们建议您注意对计划暂存的内容，主要是为了防止在提交时包含敏感信息或不需要的文件。 为避免包含此类信息，您可以使用第 5 章中介绍的 .gitignore 文件。参数 .，Unix 术语中的单个句点或点，是当前目录的简写。

使用git add命令，git知道您打算将index.xhtml上修改的最后一次迭代包含在存储库中作为修订。然而，到目前为止，Git仅仅暂存了该文件，这是通过提交获取快照之前的一个临时步骤。

Git分离了添加和提交步骤，以避免波动性，同时在记录更改的方式上提供了灵活性和粒度。想象一下，每次添加、删除或更改一个文件时更新存储库将是多么混乱、令人困惑和耗时。相反，可以批处理多个临时的和相关的步骤，例如添加

保持存储库处于稳定、一致的状态。这种方法还允许我们编写一个关于为什么要更改代码的叙述。在第4章中，我们将更深入地探讨这个概念。

我们建议您在提交之前努力对逻辑更改批次进行分组。这被称为原子提交，在后面的章节中，当你需要做一些高级Git操作时，它会帮助你。

运行git status命令显示index.xhtml的中间状态：

~~~
$ git status
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   index.xhtml
~~~

该命令报告新文件index.xhtml将在下一次提交期间添加到存储库中。

暂存文件后，下一个合乎逻辑的步骤是将文件提交到存储库。 提交文件后，它就成为存储库提交历史的一部分；为简洁起见，我们将其称为存储库历史。每次提交时，Git都会记录其他几条元数据，最值得注意的是提交日志消息和更改的作者。

一个完全合格的git commit命令应该使用活跃的语言来提供一个简洁而有意义的日志消息来表示提交所引入的更改。 当您需要遍历存储库历史记录以跟踪特定更改或快速识别提交更改而无需深入了解更改详细信息时，这非常有用。 我们将在第4章和第8章中深入探讨这个主题。

让我们为您的网站提交暂存的index.xhtml文件：

~~~
$ git commit -m "Initial contents of my_website"
[main (root-commit) 9ec7cf9] Initial contents of my_website
 1 file changed, 1 insertion(+)
 create mode 100644 index.xhtml
~~~

> 注意:进行提交的作者的详细信息是从我们前面设置的Git配置中检索的。

在代码示例中，我们提供了-m参数，以便能够直接在命令行上提供日志消息。如果您希望通过交互式编辑器会话提供详细的日志消息，也可以这样做。你需要配置Git在Git提交期间启动你最喜欢的编辑器(去掉-m参数);如果尚未设置，则可以设置

$GIT_EDITOR环境变量如下:

~~~
# In bash or zsh
$ export GIT_EDITOR=vim

# In tcsh
$ setenv GIT_EDITOR emacs
~~~

> 注意：Git将使用在shell环境变量VISUAL和editor中配置的默认文本编辑器。如果两者都没有配置，则退回到使用vi编辑器。

在将index.xhtml文件提交到存储库之后，运行git status以获得关于存储库当前状态的更新。在我们的例子中，运行的git状态应该表明没有未完成的变更要提交：

~~~
$ git status
On branch main
nothing to commit, working tree clean
~~~

Git还告诉您工作目录是干净的，这意味着工作目录中没有与存储库中的文件不同的新文件或修改过的文件。

图1-3将帮助你可视化你刚刚学习的所有步骤。

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Version%20Control%20with%20Git/Version%20Control%20with%20Git-003.png)

git add和git commit之间的区别就像你按照理想的顺序组织一群学生，以获得完美的教室照片:git add负责组织，而git commit负责拍摄快照。

### 第二次提交

接下来，让我们对index.xhtml做一些修改，并在存储库中创建一个回购历史。

将index.xhtml转换为合适的HTML文件，并提交修改：

~~~
$ cd ~/my_website

# edit the index.xhtml file.

$ cat index.xhtml

<html>
<body>
My website is awesome!
</body>
</html>

$ git commit index.xhtml -m 'Convert to HTML'
[main 60aea42] Convert to HTML
 1 file changed, 5 insertions(+), 1 deletion(-)
~~~

如果您已经熟悉Git，您可能想知道为什么我们在提交文件之前跳过了Git add index.xhtml步骤。这是因为要提交的内容可以在Git中以多种方式指定。

> TIP：各种提交方法的详细解释在git commit --help手册页中也有说明。

在我们的例子中，我们决定提交index.xhtml文件，并附加一个参数-m开关，它提供了一条消息来解释提交中的变化:'Convert to HTML'。如图1-4所示。

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Version%20Control%20with%20Git/Version%20Control%20with%20Git-004.png)

> 注意:我们使用git commit index.xhtml -m 'Convert to HTML'不会跳过文件的分段;Git将其作为提交操作的一部分自动处理。

### 查看提交

现在在存储库历史记录中有了更多的提交，您可以以各种方式检查它们。一些git命令显示单个提交的顺序，其他命令显示单个提交的摘要，还有一些命令显示您在存储库中指定的任何提交的完整细节。

git log命令会生成存储库中各个提交的连续历史记录：

~~~
$ git log
commit 60aea42eee8877738e32e71707f4b3ba5af54332 (HEAD -> main)
Author: gzkoala <bluejeams@gamil.com>
Date:   Sat Jan 7 08:41:14 2023 +0800

    Convert to HTML

commit 9ec7cf92f2f4711952a17f3f73d8498e70d82cde
Author: gzkoala <bluejeams@gamil.com>
Date:   Sat Jan 7 08:32:50 2023 +0800

    Initial contents of my_website
~~~

在上面的输出中，git log命令打印存储库中每次提交的详细日志信息。此时，您的存储历史记录中只有两次提交，这使得读取输出更容易。对于具有许多提交历史的存储库，这个标准视图可能无法帮助您轻松地遍历一长串详细的提交信息;在这种情况下，你可以提供--oneline开关来列出一个汇总的提交ID号和提交消息:

~~~
e$ git log --oneline
60aea42 (HEAD -> main) Convert to HTML
9ec7cf9 Initial contents of my_website
~~~

提交日志条目按顺序列出，从最近到最古老的1(原始文件);每个条目显示提交作者的姓名和电子邮件地址、提交日期、更改的日志消息以及提交的内部标识号。提交ID号在“内容可寻址数据库”中解释。我们将在第4章中更详细地讨论提交。

如果你想查看关于特定提交的更多细节，使用git show命令和提交ID号：

~~~
$ git show 9ec7cf92f2f4711952a17f3f73d8498e70d82cde
commit 9ec7cf92f2f4711952a17f3f73d8498e70d82cde
Author: gzkoala <bluejeams@gamil.com>
Date:   Sat Jan 7 08:32:50 2023 +0800

    Initial contents of my_website

diff --git a/index.xhtml b/index.xhtml
new file mode 100644
index 0000000..b392003
--- /dev/null
+++ b/index.xhtml
@@ -0,0 +1 @@
+My awesome website!
~~~

> 提示:如果你运行git show时没有显式的提交编号，它只会显示HEAD提交的细节，在我们的例子中，是最近的一次。

git log命令显示每次提交的更改如何包含在回购历史记录中的提交日志。如果你想查看当前开发分支的简洁的单行摘要，而不需要为git log --oneline命令提供额外的过滤选项，另一种方法是使用git show-branch命令：

~~~
$ git show-branch --more=10
[main] Convert to HTML
[main^] Initial contents of my_website
~~~

短语——more=10显示了另外10个版本，但到目前为止只存在两个版本，所以两个都显示了。(在这种情况下，默认只列出最近的提交。)名称main是默认的分支名称。

我们将在第3章中更详细地讨论分支并重新访问git show-branch命令。

### 查看提交差异

由于添加了提交，已经有了存储库历史记录，现在可以看到index.xhtml的两个修订版之间的差异。你需要收回提交ID号，然后运行git diff命令：

~~~
$ git diff 60aea42eee8877738e32e71707f4b3ba5af54332 9ec7cf92f2f4711952a17f3f73d8498e70d82cde
diff --git a/index.xhtml b/index.xhtml
index ef89853..b392003 100644
--- a/index.xhtml
+++ b/index.xhtml
@@ -1,5 +1 @@
-<html>
-<body>
-My website is awesome!
-</body>
-</html>
+My awesome website!
~~~

输出类似于git diff命令的输出。按照约定，第一次修订提交9ec7cf92f2f4711952a17f3f73d8498e70d82cde是index.xhtml的较早内容，而第二次修订提交60aea42eee8877738e32e71707f4b3ba5af54332是index.xhtml的最新内容。因此，在每一行新内容的减号(-)之后都有一个加号(+)，表示删除的内容。

注意:不要被长十六进制数字吓倒。Git提供了许多更短、更简单的方法来运行类似的命令，这样您就可以避免大而复杂的提交id。通常十六进制数字的前七个字符就足够了，如前面的git日志—一行示例所示。我们将在“内容可寻址数据库”中对此进行详细说明。

### 删除和重命名存储库中的文件

现在您已经了解了如何向Git存储库添加文件，下面让我们看看如何从一个存储库中删除文件。从Git存储库中删除文件类似于添加文件，但使用Git rm命令。假设您的网站内容中有ads .xhtml文件，并计划删除它。你可以这样做：

~~~
$ ls
adverts.xhtml  index.xhtml
$ git rm adverts.xhtml
rm 'adverts.xhtml'
$ git commit -m 'remove adverts.xhtml'
[main aa0cf85] remove adverts.xhtml
 1 file changed, 1 deletion(-)
 delete mode 100644 adverts.xhtml
~~~

与添加类似，删除也需要两个步骤:使用git rm表示要删除文件的意图，这也会显示要删除的文件。使用git提交来实现存储库中的更改。

## 使用共享存储库

到目前为止，您已经初始化了一个新的存储库，并对其进行了更改。所有更改仅对本地开发机器可用。这是一个很好的例子，说明了如何管理只对您可用的项目。但是如何在托管在Git服务器上的存储库上协同工作呢?让我们讨论一下如何实现这一点。

### 创建存储库的本地副本

您可以使用git clone命令创建存储库的完整副本或克隆。这是您与其他人协作的方式，对相同的文件进行更改，并与来自同一存储库的其他版本的更改保持同步。

为了本教程的目的，让我们简单地从创建现有存储库的副本开始;然后我们可以对比相同的例子，就像它在远程Git服务器上一样：

~~~
$ git clone my_website/ new_my_website
Cloning into 'new_my_website'...
done.
~~~

尽管这两个Git存储库现在包含完全相同的对象、文件和目录，但仍有一些细微的差异。你可能想用以下命令来探索这些差异：

~~~
$ ls -lsa my_website new_website
...
$ diff -r my_website new_website
...
~~~

在这样的本地文件系统上，使用git clone来创建存储库的副本与使用cp -a或rsync非常相似。相反，如果你是

从Git服务器克隆相同的存储库，语法如下：

~~~
$ git clone https://git-hosted-server.com/some-dir/my_website.git new_website Cloning into 'new_website'...
remote: Enumerating objects: 2, done. remote: Counting objects: 100% (2/2), done.
remote: Compressing objects: 100% (103/103), done.
remote: Total 125 (delta 45), reused 65 (delta 18), pack-reused 0 Receiving objects: 100% (125/125), 1.67 MiB | 4.03 MiB/s, done.
Resolving deltas: 100% (45/45), done.
~~~

克隆存储库之后，可以修改克隆的版本，进行新的提交，检查其日志和历史记录，等等。它是一个具有完整历史的完整存储库。请记住，您对克隆存储库所做的更改不会自动推送到存储库上的原始副本。

如图1-5所示：

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Version%20Control%20with%20Git/Version%20Control%20with%20Git-005.png)

尽量不要被输出中看到的一些术语分散注意力。Git支持更丰富的存储库源集，包括网络名称，用于命名要克隆的存储库。我们将在第11章解释这些形式和用法。

## 配置文件

Git配置文件都是.ini格式的简单文本文件。配置文件用于存储多个git命令使用的首选项和设置。有些设置代表个人偏好(例如，应该是一种颜色。)，其他的对于存储库的正确运行很重要(例如，core repositoryformatversion)，还有一些对git命令的行为进行了一点调整(例如，gc.auto)。与其他工具一样，Git支持配置文件的层次结构。

### 配置文件层次结构

图1-6显示了Git配置文件优先级递减的层次结构:

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/Version%20Control%20with%20Git/Version%20Control%20with%20Git-006.png)

.git/config

特定于存储库的配置设置使用--file选项或默认情况下操作。您还可以使用--local选项写入该文件。这些设置具有最高优先级。

~/.gitconfig

使用--global选项操作特定于用户的配置设置。

/etc/gitconfig

如果您对gitconfig文件有适当的Unix文件写权限，则可以使用--system选项操作系统范围的配置设置。这些设置的优先级最低。根据您的安装，系统设置文件可能在其他地方(可能在/usr/local/etc gitconfig)或者可能完全不存在。

例如，要存储将在所有存储库的所有提交中使用的作者名称和电子邮件地址，请配置用户名和用户的值。邮件地址是$HOME/。使用gitconfig——global的Gitconfig文件:

~~~
$ git config --global user.name "Jon Loeliger"
$ git config --global user.email "jdl@example.com"
~~~

如果你需要设置一个特定于存储库的名称和电子邮件地址来覆盖--global设置，只需省略--global标志或使用--local标志显式：

~~~
$ git config user.name "Jon Loeliger"
$ git config user.email "jdl@special-project.example.org"
~~~

你可以使用git config -l(或长形式--list)列出在完整的配置文件集中找到的所有变量的设置：

~~~
$ git config -l
user.name=gzkoala
user.email=bluejeams@gamil.com
core.repositoryformatversion=0
core.filemode=true
core.bare=false
core.logallrefupdates=true
~~~

> 提示:当指定命令git config -l时，添加选项--show-scope和--show-origin将有助于打印配置的各种来源!在你的终端上用git config -l --show-scope --show-origin试试。

因为配置文件是简单的文本文件，你可以用cat查看它们的内容，也可以用你喜欢的文本编辑器编辑它们：

~~~
$ cat .git/config
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
~~~

> 注意:根据您的操作系统类型，配置文本文件的内容可能会有一些细微的差异。其中许多差异允许不同的文件系统特征。

如果您需要从配置文件中删除设置，请使用--unset选项和正确的配置文件标志：

~~~
$ git config --unset --global user.email
~~~

Git为您提供了许多配置选项和环境变量，它们经常出于相同的目的而存在。例如，您可以为编辑器设置一个值，以便在编写提交日志消息时使用。根据配置，调用遵循以下步骤:

1. GIT_EDITOR环境变量
2. CORE.EDITOR配置选项
3. VISUAL环境变量
4. EDITOR环境变量
5. vi命令

有几百个以上的配置参数。我们不会用它们来烦你们，但会在后面指出一些重要的。在git配置手册页上可以找到一个更广泛(但仍然不完整)的列表。

## 配置别名

Git别名允许您使用简单且易于记忆的别名来替换经常键入的常见但复杂的Git命令。这也为你省去了记忆或输入那些冗长命令的麻烦，也让你避免了键入错误的挫败感:

~~~
$ git config --global alias.show-graph 'log --graph --abbrev-commit --pretty=oneline'
~~~

在本例中，我们创建了show-graph别名，并使其可用于我们创建的任何存储库。当我们使用命令git show- graph时，它将给我们相同的输出，当我们输入带有所有这些选项的长git log命令时。

# 总结

关于Git是如何工作的，您肯定会有许多未解之谜，即使您已经学习了到目前为止的所有知识。例如，Git如何存储文件的每个版本?提交的真正组成部分是什么?那些可笑的提交数字是从哪里来的?为什么叫" main " ?“分支”是我想的那个吗?这些都是好问题。我们所涵盖的内容让您大致了解了在项目中经常使用的操作。你的问题的答案将在第二部分详细解释。

下一章定义了一些术语，介绍了一些Git概念，并为本书其余部分的课程奠定了基础。
