# 1. 前言

虽然开源软件(OSS)随着开发⼈员⾃下⽽上的快速采⽤⽽有机地出现，但使⽤开源的企业随着时间的推移意识到，他们需要创建治理结构和“护栏”，以确保合规使⽤这种新兴的范式转换技术。使OSS在加速技术创新⽅⾯如此强⼤、对开发⼈员和“解决问题”⼯程师如此有吸引力的元素。透明、快速迭代、协同创新这些是法律团队经常关注的特征，与传统的技术发展战略背道⽽驰。

很快就清楚地表明，开源不仅是⼀个可行的选择，⽽且是技术创新的关键途径。企业意识到他们需要适应这种新的创新⽅式，否则就有落后的⻛险。在这种背景下，最初的组织⼯作重点是确保开发⼈员遵守开源许可证并保留使⽤中的OSS库。在技术组织中，开源评估人员是⽂化不可分割的⼀部分，并且经常嵌⼊产品中；对话通常远远超出许可证合规性，⽽是探索这些公司如何利⽤开源社区来加速和改进产品开发，同时保持商业优势。在这两个轴上，开源计划办公室(OSPO)的种⼦发芽成促进合规开源使⽤的正式项⽬。随着时间的在新成⽴的OSPO的⼤力⽀持下，我们⻅证了更多的开源采⽤、贡献和社区范围内的参与。

TODO Group的存在是为了促进OSS使⽤和社区建设⽅⾯的最佳实践。这越来越意味着授权企业和组织创建有效的OSPO计划。我们发布了许多⼴受好评的关于如何构建OSPO计划的指南和⼯具包。本出版物旨在提供⼀个更⼴泛的框架，以了解OSPO的原型以及我们在与数千家企业和组织合作时所看到的成熟阶段，因为他们已经完成了开源之旅。我们还想捕捉⼀些真实的“⽤⼾旅程”，⽤真实的⼈的真实声⾳讲述他们的真实体验。我们希望他们的⻅解能让现有和潜在的OSPO领导者了解促进开源的细微差别。这项⼯作为这些旅程提供了指导和路线图，因为OSPO的概念和结构随着开源运动⽽不断发展。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0315/175927_b53cc602_8894319.png "Screenshot from 2022-03-15 17-32-38.png")

**摘要**

在我们对开源实践的第四次TODO Group调查中，我们发现OSPO正朝着拥有更多专⽤资⾦和资源的更专业的组织发展。这种演变符合⼤⼩组织中OSS和开发实践持续增⻓和接受的持续主题。为了更深⼊地了解OSPO的演变，我们采访了著名OSPO项⽬的领导者，包括⼀些最具影响力的技术公司，如红帽、微软和VMware，以及⼀些最具标志性的交通品牌和最⼤的媒体和娱乐公司之⼀。我们询问了他们的计划是如何开始的以及它们是如何演变的。基于这些访谈和调查数据，我们制定了⼀个五阶段的OSPO成熟度模型，从(1)偶尔使⽤OSS到(2)解决合规性和许可问题到(3)⿎励参与到(4)贡献代码，最后到(5)孵化和开源有意义的项⽬。在本报告中，我们详细介绍了每个阶段，并开发了⼀些OSPO组织的原型。作为该过程的⼀部分，我们对三个不同行业（媒体、⾦融服务和交通）的三个OSPO的演变进行了三个案例研究，每个案例都构建为OSPO各个阶段的旅程。在OSPO运动⼆⼗多年后，OSPO的作⽤已发展成为专业知识的核⼼来源，并在全球具有前瞻性思维的公司制定和实施技术战略⽅⾯发出强有力的声⾳。

# 2. 介绍

OSPO的兴起⼤致反映了OSS在当今世界组织内构建和运行最重要的技术应⽤程序的扩散。精⼼设计的OSPO是中⼼能力的⽤于组织的开源运营和结构。它的⻆⾊可以包括设置代码使⽤、分发、选择、审计和其他政策，以及培训开发⼈员、确保法律合规以及促进和建⽴有利于组织战略性的社区参与。 OSPO概念现在⼤约有20年的历史，但在过去⼗年左右才真正开始加速。⼤多数著名的技术基础设施公司（例如，亚⻢逊、VMware、思科）和消费技术公司（例如，Apple、Google、Facebook 和 Twitter）都有OSPO或正式的开源计划。所有⼈都在⿎励他们的员⼯为对其业务和安全具有战略意义的开源项⽬做出贡献。

OSPO在早期最初专注于许可证合规性，如今在组织内部经常发挥更⼴泛的作⽤。OSPO通过促进最佳OSS实践和参与OSS社区来提⾼开发⼈员的效率，从⽽对开发⼈员和其他员⼯进行有关 OSS的教育。随着时间的推移，OSPO已经从参与现有项⽬发展为向更⼴泛的社区⽣成和启动项⽬。⾼层管理⼈员更有可能承认开源技术在加速创新和在多个受益者之间分摊软件开发成本⽅⾯发挥的关键作⽤。

OSPO的任务在关键⽅⾯取得了进展，包括：

- 创建内部框架和⼯具，以有效和⾼效地使⽤开源项⽬代码
- 提供战略和战术指导，说明组织应如何指导员⼯参与开源以及⽀持哪些项⽬
- 评估开源项⽬并提供战略，将项⽬纳⼊组织的⻓期技术计划的⻛险和回报指南，重点关注开发⼈员的经验和效率

随着这种演变，⼈们更加依赖OSS和开发指标来衡量OPSO对组织的影响、每个组织对开源项⽬的影响以及由OSPO及其上级组织创建和承保的项⽬的总体健康状况. OSPO的形成类似于组织⾸次开始建⽴CISO作为对安全事件的反应。建⽴这些安全能力中⼼的组织为更美好的未来保护和武装⾃⼰。那些没有承担后果的人，由于安全措施不佳，造成了财务上的影响。

简⽽⾔之，随着时间的推移，OSPO所关注的内容已经与他们的新⻆⾊相匹配。本报告的⽬的有两个：介绍最新的年度OSPO调查的主要发现，并通过与领先的从业者和专家的访谈为这些结果提供背景。我们在由Linux基⾦会主办的组织TODO Group 的主持下进行了调查和采访。 TODO是⼀个开放的组织团体在实践、⼯具和其他⽅式上进行协作，以运行成功和有效的开源项⽬和程序。

## 调查⽅法和受访者构成

2021年6⽉10⽇⾄6⽉29⽇，TODO⼩组与Linux Foundation Research和New Stack合作进行了⼀项调查，以更好地了解OSPO的形成、运营和进化。这是TODO Group连续第四年进行这项调查。我们通过社交媒体和直接电⼦邮件向Linux 基⾦会、TODO 集团和新堆栈订阅者列表征集受访者。最终数据集包括来⾃1,141名调查参与者的回复。我们从⾄少有两名员⼯的 932个组织中得出结论。受访者的构成与往年略有不同，将⾃⼰描述为“个体经营者”或“不⼯作”。在2021年的调查中，与往年相⽐，为科技公司⼯作的受访者减少了。受访者来⾃各行各业，包括教育、电信、媒体、⾦融服务、政府、交通和汽⻋、医疗保健和零售。按行业划分的受访者百分⽐增幅最⼤的是政府、教育和零售业。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0315/180355_0f34e739_8894319.png "Screenshot from 2022-03-15 18-03-42.png")

## 主要调查结果

随着OSPO调查的受访者数量在2021年显着增加，调查受访者的构成发⽣了变化，以反映整体经济，⽽不仅仅是技术（尽管回复仍然以技术为主）。与2019年相⽐，对OSPO ⻆⾊、优先事项和价值观的看法出现了⼀些趋势。尽管所有组织规模的组织中，拥有OSPO计划的组织的响应百分⽐都有所下降，但在⼤型组织中下降幅度最⼩。

这种下降可能反映了调查构成的变化：它包括教育和政府等行业，这些行业的开源项⽬还处于起步阶段。这种下降也可能与COVID-19带来的经济和⼈员配置挑战有关。

即便如此，OSPO运动仍有发展空间。显然，OSPO倡导者必须更有效地传达专⻔创建OSPO和使⽤OSS以及更⼴泛地为开源社区做出贡献的价值。

调查显⽰，组织使⽤的OSS多于他们对开源项⽬的贡献，⽽做出回应的组织并不完全了解OSPO：

- 19%的⼈表⽰他们从未听说过OSPO
- 28%的⼈表⽰他们认为OSPO没有商业价值
- 35%的没有OSPO的组织表⽰他们没有考虑开设OSPO

该调查还揭⽰了充满希望的信号。与去年相⽐，受访者认为，由于宏观经济状况，本财年为其公司的开源计划提供的资⾦将增加的可能性是去年的两倍。为此，51%的⼈表⽰很有可能或有可能增加资⾦。调查数据显⽰，从54%增加到63%的受访者表⽰OSPO对其⼯程或产品团队的成功⾮常或极其重要。OSPO也通过正式的结构变得更加专业。 2021年的调查发现，58%的OSPO 是正式结构化的，⾼于2020年的54%。在调查的开放式部分中，受访者强调了OSPO的许多有价值的好处，例如提⾼代码质量、更好地利⽤OSS⼯具（如连续集成/持续交付 (CI/CD) 管道，以及外部协作（开源）和内部协作（内部源）之间的积极联系。

OSPO的主要职责发⽣了⼀些变化。维持开源许可证合规审查和监督的调查参与者从68%下降到59%，认为这是⼀项主要责任。对于⼤型⾮科技公司，合规仍然是最常被提及的主要责任，占 86%。将与开发者社区互动作为主要责任的⽐例从48%上升到56%。

在与⽣态系统参与和宣传相关的⼏个积极指标中，对开发⼈员关系和参与的重视将内部开源项⽬的外部贡献从38%增加到47%。

# 3. 五阶段OSPO成熟度模型

随着OSPO的激增并变得越来越普遍，这些计划已经成熟。通过将与OPSO领导者和专家的对话映射到OSPO调查结果，我们开发了⼀个OSPO成熟度模型来描述OSPO的典型演变。该模型是通⽤的：组织的规模和类型会影响OSPO的成熟度。在较⼤的组织中，多个业务部⻔可能会开发不同的开源⽅法，每个⽅法都有不同的技术⽂化；⽽纯数字技术公司更有可能更早地使⽤OSS并为之做出贡献，并且更容易接触到开源技术和概念。

考虑⼀下企业基础架构软件提供商VMware。它的⼯程师与⽹络、云和其他关键领域的许多开源社区合作并为之做出贡献，仅仅是因为他们知道使⽤开源进行构建可以为社区和VMware的客⼾带来更好的结果和更⾼的互操作性。相⽐之下，红帽是第⼀家上市的开源公司。它在OSS上建⽴了整个业务实践，压缩了成熟度⽣命周期，实际上，使整个公司成为了⼀个OSPO。如今，红帽将更多资源⽤于早期⽣命周期活动，例如教育内部利益相关者（例如，销售团队、营销⼈员、新⼯程员⼯）和促进上游社区的协作。对于我们研究中的⼤多数其他公司，成熟度模型的⼀般阶段在消耗、贡献、协作和参与以及领导力⽅⾯密切地映射了组织的OSS轨迹。我们采访的⼀些组织现在包括开源参与和使⽤的特定指标。

这些指标可能包括OSS项⽬中的⼯程参与率（拉取请求、评论、提交）、开源活动的出勤率和参与度、撰写的博客⽂章、发表的演讲以及参与开源项⽬的闲暇时间等等。更⾼级的开源组织可能有关于项⽬成功增⻓的指标，这些项⽬由他们⾃⼰的⼯程团队发起或部分创建。⼀些领先的组织，例如Comcast、VMware和Red Hat，已经构建或正在构建⾼级度量和测量⼯具。

也就是说，即使是⼀些跟踪指标的复杂组织也不会明确使⽤指标来跟踪或设置OSS⽬标。以微软为例，该组织曾经⼏乎完全专注于专有软件，但现在已成为开源项⽬的主要⽀持者，并在其⾃⼰的产品中⼴泛使⽤OSS。 “我们专注于让我们的开发⼈员更容易使⽤OSS，我们⿎励他们回馈他们所依赖的项⽬。我们跟踪整体参与情况，但开源计划办公室确实在个⼈或团队层⾯设定了⽬标，”微软OSPO前负责⼈Stormy Peters 说。 “我们的开发⼈员可以⾃愿将他们的公司ID与他们的GitHub登录名关联起来，这使我们能够衡量公司层⾯的参与度。”1在⼤多数情况下，系统化的指标收集和分析发⽣在采⽤的后期阶段，即OSS成为企业和⼤型组织的技术路线图和战略的关键要素，同时伴随着OSPO计划、预算和员⼯的增⻓。

![输入图片说明](https://images.gitee.com/uploads/images/2022/0315/183147_0cff8e05_8894319.png "Screenshot from 2022-03-15 18-31-36.png")

## 阶段0：特定采⽤开源

关于开源库和框架。 Red Hat报告称， “90%的IT领导者正在使⽤企业开源。” Synopsys等软件组合分析供应商确定超过75%的代码库包含开源组件。

换句话说，⼏乎每个组织都在使⽤开源。然⽽，最早的采⽤形式是临时的，由开发⼈员使⽤现成的⼯具和技术解决问题。这种“临时采⽤”通常意味着很少考虑默认情况下的许可合规性或使
⽤开源和分发使⽤开源组件构建的产品的⻓期影响。在⼤多数情况下，⼀些⼯程师正在积极寻找开源，⽽⼯程组织的其他成员可能会偶然使⽤开源代币，但并不认为其活动依赖于开源。因此，该组织既没有专注于开源的集中团队，也没有组织的顶级开源战略。这些⾄关重要，因为⼀旦采⽤这些开源组件，默认情况下，这些组件就会成为组织软件供应链的⼀部分，这使得战略⽅法变得更加必要。

## 阶段1：提供OSS合规性、库和开发⼈员教育

⼀般来说，当⼀个组织意识到其⼈员正在使⽤⼏乎所有⼯程和开发部⻔和职能部⻔的开源产品和代码时，就会形成⼀个OSPO。这种使⽤通常是内部的，⽽不是客⼾或⽤⼾的产品或服务的⼀部分。实际上，任何拥有⼤量IT功能和先进的在线或以应⽤程序为中⼼的组织都使⽤开源，因为开源在整个技术堆栈中⽆处不在从Linux服务器和MySQL数据库到Node.js等编程语⾔和Python和前端框架，如React和Vue.js。

在这个早期阶段，组织经常为OSPO使⽤许多不同的名称。 IBM最初将其程序化开源⼯作称为“开源指导委员会”，然⽽，在所有情况下，处于第1阶段的组织都认识到OSS是其业务和技术战略的关键部分。他们明⽩OSS项⽬的安全实践与专有软件公司的不同。例如，OSS项⽬的披露规则往往⽐专有项⽬的披露规则更严格。因此，他们必须识别其法律和安全⻛险。⻛险缓解策略包括谨慎的许可、开发⼈员教育和严格的盘点。

### 管理法律⻛险和许可

组织的法律团队或技术负责⼈倾向于启动OSPO的第1阶段开发，以确保其员⼯（以及承包商、供应商等）都根据其许可条款使⽤OSS，并且组织的OSS消费不会使其合法化⻛险。有⼏⼗个OSS 许可证在使⽤中。

在2020年的调查中，受访者将合规列为⼤公司OSPO的最⼤优势，⽽合规仍然是中型公司的第⼆⼤优势。 “公司⼀开始通常会很困惑。没有针对许可证合规性的政策，开发⼈员会做他们认为正确的事情，”弗里德里希-亚历⼭⼤⼤学埃尔兰根纽伦堡开源软件教授Dirk Riehle 说。他补充说道：曾经⾛进⼀家公司，⼀位开发者说：我们没有开源政策。另⼀个打趣道：我们有，⽽且是：没有开源。第三个皱着眉头评论道：“你在说什么？⼀段时间以来，我们⼀直在为开源项⽬做出贡献。”这并不罕⻅。他们最终将成⽴⼀个开源项⽬办公室，负责处理开源的使⽤和贡献。

虽然OSS⽤⼾⼀直考虑合法合规，但⼀些OSS贡献者设计了新的许可证，以阻⽌⼤型云提供商创建基于开源项⽬的专有服务。其中最突出的是Affero通⽤公共许可证(AGPL)。公司可能会使⽤根据本许可条款发布的OSS向其客⼾提供专有软件即服务 (SaaS)，但如果AGPL条款确实违反了许可，则OSS的创建者可能有理由起诉该公司没有明确区分内部和外部交付。许多企业在单位之间也有内部财务收费系统，进⼀步模糊了付费服务和内部服务之间的界限。

### 教育开发者

为了保持合规性，处于OSPO成熟阶段1的组织创建了教育计划，以帮助其开发⼈员决定何时使⽤OSS来创建新产品或服务。 “许多没有接受过开源教育的开发⼈员认为，由于他们没有购买软件，因此没有签署合同，因此没有涉及许可，”VMware 开源营销和战略总监Suzanne Ambiel说。 “开源软件可能是免费的就像⽆价之宝⼀样如果以不合规的⽅式使⽤，它也可能代价⾼昂。OSS总是带有许可证。任何OSPO最重要的⻆⾊之⼀是确保开发⼈员了解不同许可选择的含义。

通过开发⼈员教育，⾼级管理⼈员经常承认 OSS 的价值和重要性。在此类程序中，开发人员学习：

- 不同许可证类型的细微差别
- 引⼊新OSS的正式产品
- 不合规OSS消费的真正⻛险，包括在没有正式许可的情况下使⽤项⽬或代码中的OSS产品
- 使⽤贡献者许可协议(CLA)来涵盖组织为开源做出贡献的开发⼈员

有时组织会在此阶段引⼊正式的CLA政策。它还可以为判断OSS项⽬的健康状况提供指导，作为其决定在组织的技术堆栈或基础设施中使⽤哪个OSS的标准的⼀部分。

### 清点软件清单

开发者获取软件清单开发⼈员可能会临时部署OSS，⽽⽆需系统地对他们的⼯作进行分类。法律团队和技术领导倾向于推动组织中使⽤的所有OSS。迎。这样的清单列出了组织代码存储库（例如 GitHub、GitLab）和系统中的OSS。第1阶段的组织建⽴了特定的软件清单流程，以创建组织范围内的软件材料清单 (SBOM)。借助此清单，法律团队（通常与 OSPO 团队合作）可以持续监控OSS的使⽤情况并标记法律、安全或其他项⽬⻛险。通过详细的SBOM，⾸席技术官 (CTO) 或⾸席信息官 (CIO) 等技术领导可以识别并密切监控最关键的业务⽤途并保护组织。

## 阶段2：宣传OSS使用和参与生态系统

在组织认识到OSS的价值以及对合规性、教育和SBOM的需求之后，他们开始意识到使⽤OSS的经济利益并寻求扩展它。第2阶段的OSPO会创建内部机制，例如推⼴使⽤已获批准的OSS产品的⼤使、关于良好OSS合规的教育计划，以及⽤于OSS技能建设和认证的技术培训或学费报销。通过这些举措，组织可以扩⼤其对OSS的使⽤，并扩⼤其信息，即OSS不仅重要，⽽且⽐专有软件产品更可取，⽽且更受欢迎。

员工教育包括制定与 OSS 项目交互的最佳实践，例如如何请求功能、提交错误报告和贡献基本代码。 在此阶段，组织会加强其协作能力并体验 OSS 项目和社区的社交生活。 在这一点上，OSPO 向员工和经理传达了为 OSS 做出贡献而不仅仅是消费 OSS 的重要性。 这种外展包括倡导和推动活动赞助、预订项目负责人和维护人员作为公共编码论坛的发言人或小组成员，以及为关键任务 OSS 项目确保组织资源（例如，人才、资金）。

> 最初由出版商和技术风险投资家 Tim O'Reilly在2000 年创造，内源（InnerSource）这个术语已经用来描述公司内部的协作创新努力，这些努力模仿了经典OSS社区的一些（但不是全部）特征。InnerSource倡导者寻求形成内部社区来维护和开发多个团队可用于多个应用程序的软件。这种方法不同于软件开发的孤立模型，团队在项目上工作，有时会重复工作。实际上，内源是对开源实践的认可，并试图采用那些可以加速公司内部软件创新的实践。然而，作为一项规则，内部源代码软件通常是专有的，不打算通过开源许可证发布。也就是说，内源软件可能是以后开源相同软件的测试运行。PayPal和Comcast等公司拥有活跃的内部资源计划和内部资源传播者。内部源经常与OSPO管理密切合作，甚至在其下工作。这两种方法通常被视为软件开发连续体上的相邻点。

对于组织⽽⾔，积极和可⻅的参与会产⽣多种好处：更好的知名度、更好的声誉、更有吸。为此，许多⾮技术组织在著名的 OSS 活动上购买展位，以与这些社区进行更多互动，并招募喜欢在OSS⽣态系统中⼯作的开发人员。

活跃于开源领域的技术公司可能会将教育计划扩展到希望与OSS社区和供应商互动的客⼾。 “我们收到了很多客雇主。就如何参与开源或如何为项⽬做出贡献或与我们合作寻求帮助和指导，”RedHat开源⾼级总监 Deborah Bry说道。

在技术领域之外，很少有组织能够指派全职员工从事开源工作，但他们正在这么做。例如，康卡斯特和彭博社都有员工全职从事OSS项目。在生命周期的这个阶段，OSPO开始探索如何简化开发人员使用OSS的过程。这样的开发人员效率可能包括简化CLA，将具有可接受的许可类型的OSS添加到票务系统以实现快速审批，促进OSS架构和软件的重用(内部源代码的变体)，以及标准化库选择和开源开发工具，从而将OSPO和平台运营职责结合起来。

在这个阶段，组织会向ospo寻求如何积极参与开放生态系统的指导。“你必须确保你回馈的和你得到的一样多。你不希望人们认为你只是把开源货币化，而没有回馈社区，”Futurewei Technologies OSPO负责人Chris Xie说。“我们非常重视这一点，比以往任何时候都更加重视。”电信等受监管部门的公司也必须了解本国的出口法律，应对政治紧张局势，以保护OSS社区，并避开国际纠纷。“我们一直希望确保我们的贡献是真正开放的，造福社区，造福整个行业，”Xie解释道。

通常在OSPO成熟度周期的第2阶段（或者有时在第1阶段，如果公司是软件或核⼼技术公司），OSPO开始为其开发⼈员简化和优化开源出站源贡献。在早期，请求和获得对外参与批准的过程通常是临时的和痛苦的。SAP OSPO 的⾸席架构师 Michael Picht 说：“在我们建⽴ OSPO 时，我们⾸先考虑的事情之⼀就是捐款流程。” “使⽤Word、Excel 和电⼦邮件，这个过程根本没有⾃动化。当我们打开OSPO时，我们做的第⼀件事就是简化流程并实施端到端⼯具⽀持。我们将GitHub 问题⽤于不同的流程步骤。”

## 阶段3：托管OSS项目和社区成长

在第3阶段，组织发起并托管或充当OSS项⽬的主要发起⼈。他们将为⼀个项⽬指定⼀名或多名全职人员，并承担培育项⽬社区和确保其健康的责任。他们不会将这种级别的组织承诺与决定开源项⽬的个人员⼯混淆。在第3阶段，组织领导者⽀持在公共领域孵化和启动开源项⽬，因为他们了解这些项⽬如何使他们的组织受益。此类项⽬往往会在关键能力上提供更好的性能和经济性，这些能力可能是对组织的价值主张⾮核⼼，但对其技术基础设施⾄关重要。

此外，创建和启动开源项⽬的组织在开源社区中建⽴了⼴泛的信誉；从事开源技术的可能性对许多开发⼈员很有吸引力。我们采访过的⼤多数OSPO都将招聘新的⼯程⼈才和留住现有⼈才作为开源⼯作的关键动力。在Linux Foundation Research最近的⼀项研究中在⾦融服务行业，53%的贡献者表⽰他们为OSS做出贡献是因为“它很有趣”。

⽤全职员工和资⾦⽀持项⽬是开源游戏的真正面目。跨越这⼀⻔槛并成功启动多个开源项⽬的组织开发了内部资源和流程，可以孵化并确保这些项⽬在启动后的成功。 OSPO不仅仅是项⽬形成和启动的守⻔⼈和导师；他们教育项⽬创建者培养健康的开源⽣态系统的要求，并指导项⽬负责⼈，让他们为OSS项⽬所需的更公开的领导⻆⾊做好准备。

随着OSS组织的成熟，其OSPO会开发内部流程、手册、检查表、⼯具和其他机制来审查、组织和运营开源项⽬，并准备和指导他们的领导者。⼀些OSPO更愿意在主要开源基⾦会或合作组织（如TODO集团）的协助下启动项⽬，以增强能力或提供基础设施、战术援助和其他资源。这种偏好占⽤的资源较少，但会将项⽬的控制权交给更⼴泛的社区。

## 阶段4：成为战略决策合作伙伴

在这个成熟阶段，OSPO成为技术决策的战略合作伙伴，帮助指导选择并形成对项⽬的⻓期承诺。在第4阶段，CTO和其他技术领导者咨询OSPO及其领导层，了解哪些开源技术可以依赖以及哪些决策标准⽤于判断开源项⽬。由于主要的开源技术选择往往会产⽣⼤量的⼆级和三级成本并影响上游和下游技术以及招聘计划，因此开源项⽬的选择成为⼀项重⼤的商业决策。

从⼴义上讲，OSPO 在第4阶段提供三种类型的战略指导。⾸先，OSPO建议CTO和技术领导层采⽤或从组织的技术堆栈中删除哪些开源技术。鉴于当今有许多OSS选项⼤多数主要类别的软件都有数⼗种选择，如图3所⽰OSPO可以提供对OSS趋势的洞察，例如不同语⾔的流行度、API 设计或不同 NoSQL 数据库的功能.在这个⻆⾊中，OSPO 成为 CTO 的内部技术顾问和 OSS 的内部专家。

云原生Landscape
资料来源：云原生计算基金会，2022年2月22日访问

![输入图片说明](https://foruda.gitee.com/images/1672657065746701874/ff271b0a_8894319.png "OSPO Report15.png")

在第⼆种类型的战略指导中，OSPO带头对构成可接受的OSS项⽬的内容进行基准测试。 OSPO经常评估项⽬的行为和绩效，特别是限制使⽤的许可类型的变化，或项⽬路线图的突然变化，以确定项⽬经理是否考虑到社区的最佳利益。

- 它的行为准则是什么?违反它的后果是什么?
- 它的治理结构是什么，这种结构是否能确保独立性?
- 响应pull请求或漏洞归档需要多长时间?
- 项目发布新版本的频率?
- 是一方(公司或组织)还是整个社区控制项目?
- 项目有多少贡献者?这个数字是如何随时间变化的?

第三种指导是帮助组织理解和驾驭项⽬政治，例如当多个有影响力的参与者试图指导⼀个项⽬时保持中⽴⽴场，或者阐明社区成员潜在的政治考虑。在更⾼的层⾯上，OSPO 可以通过培养超越国界和政治领域的个⼈和⼯作关系，帮助公司在技术⺠族主义上保持中⽴姿态并弥合政治分歧。随着这些领域成为开源中重要的中⽴空间，这种价值越来越多地延伸到基⾦会和⾮营利组织的工作中。

根据Red Hat的Deborah Bryant的说法，她的OSPO必须管理参与开源基⾦会⼯作的成本，在赞助和专⻔⼈员担任领导职务之间。 “我们发现，我们需要花更多时间在⼀些集中管理和管理我们对软件基⾦会的参与，以确保我们获得投资回报，并定期重新评估我们的参与，”她说.在这个职位上，OSPO拥有数百万美元的基⾦会预算，参与OSS⽣态系统形成和增⻓的战略重要性与对基⾦会和⾮营利组织的货币投资是平行的。在这个阶段，我们倾向于看到OSPO的快速增⻓。根据VMware的Ambiel的说法：今天，OSPO的主要⽬标之⼀是帮助在教练⽅⾯的最佳实践以及如何成为⼀名优秀的开源公⺠。当你在开源社区时，你就是在参与开放每个⼈都可以看到你在做什么。⼀个组织发挥其最好的⼀⾯是很重要的。 OSPO帮助⼈们始终如⼀地⾃信地做到这⼀点，⽆论是在会议上发⾔，还是为参与⼤型项⽬社区（如 Kubernetes）贡献⼀个⼩型库。

# 4. OSPO的原型

![输入图片说明](https://foruda.gitee.com/images/1672657530774533561/0b504483_8894319.png "OSPO Report16.png")

经常出现的⼀个问题是，OSPO的各种类型是什么，它们有何不同？在⼀定程度上，任何⾃称为OSPO的组织都可能表明该组织已达到成熟阶段和临界质量，其中OSPO共享关键特征：

- 员工的任务是促进和培养OSS的使用。
- 组织有关于OSS的使用和生产的正式政策。
- 高管们认识到OSS和更广泛的开源是重要的战略资产。
- 大量员工正在为开源项目贡献代码。
- 制定流程、程序和工具来简化和促进开源消费和参与。

在本⽂中，我们采访了主要在总部位于北美、欧盟和亚洲的⼤型企业⼯作的OSPO领导⼈。由于样本量有限，我们⽆法正确观察⼩型组织、⾮公司实体和总部位于其他领域的组织的原型。 （更详细、更细致的原型研究将有利于 OSPO 运动）。根据我们为本⽂进行的采访，我们确定了⼀些推动OSPO行为差异化的⼴泛原型。

## 行业合作

这种原型中的OSPO将开源视为⼀个平台，不仅⽤于⼀般技术开发，⽽且是通过分摊成本和创新满⾜特定行业需求的特定行业变得更加⾼效的⼀种⽅式。在欧盟，许多主要汽⻋公司已经形成了⼀个松散的OSPO协调联盟，该联盟优先考虑汽⻋的关键OSS计划，并就这些计划的软件开发进行合作。该联盟还致力于解决⾮行业特定的OSS问题，例如创建和维护⼀组⼯具以⾃动化 OSS 合规性和验证。

> 这里输入引用文本将开源视为特定行业通过分摊成本和创新来满⾜行业特定需求的⼀种⽅式 - 欧洲联盟

## 跨行业合作

具有这种原型的OSPO渴望致力于解决跨行业的基础技术问题。这项⼯作通常采⽤其他⼯具的形式，以⾃动消费和合规开源编程语⾔和框架（如 JavaScript 和Node.js）上的开源⼯作。例如，Bloomberg与Microsoft合作，为TypeScript做出了贡献，并创建了⼀个更好的⼯具结构，使Bloomberg⼯程师能够更轻松地回馈代码。

> 渴望研究跨行业的基础技术问题。这项⼯作通常采⽤其他⼯具的形式来⾃动消费和合规开源编程语⾔和框架上的开源⼯作 - 彭博与微软合作为TypeScript 做出贡献

## ⼤项⽬促进者

这些是形成或促进形成的罕⻅的OSPO组织内部的⼤型、复杂的开源项⽬，然后将它们作为公开可⽤的项⽬启动。此类项⽬的开销和承诺⽔平很⾼。代码的持续开发和社区的发展都需要⼤量的时间和⾦钱投资。出于这个原因，⼤多数OSPO并不寻求从其组织中启动⼤型项⽬。相反，当⼀个⼤项⽬启动时，公司通常会将其捐赠给基⾦会作为启动的⼀部分。

这个⼤项⽬还为OSPO的上级组织发挥了关键的战略技术作⽤。例如，康卡斯特孵化了Apache流量控制项⽬，这是Apache 基⾦会的顶级项⽬。流量控制是康卡斯特软件和服务堆栈中的关键技术组件，⽤于向现场客⼾提供关键任务内容。

> 在组织内部形成⼤型、复杂的开源项⽬，然后将它们作为公开可⽤的项⽬启动 - Comcast孵化了Apache Traffic Control项⽬

## 开源优先

当今许多OSPO的⼀项关键⼯作是帮助公司及其技术团队优先考虑OSS使⽤，并使开源成为任何技术计划的默认⾸选。这些OSPO倾向于与CTO和公司战略家密切合作，以绘制开源项⽬和能力。开源优先OSPO敏锐地意识到Copyleft的趋势许可和其他限制性形式的开源许可。

> 帮助公司及其技术团队优先考虑OSS的使⽤，并使开源成为任何技术计划的默认⾸选 - OSPO与CTO和公司战略家密切合作以映射开源项⽬和能力

## 技术战略专家

此OSPO原型与开源第⼀原型密切相关且经常重叠，在评估可行的开源技术和帮助组织的CTO和⼯程副总裁制定技术路线图⽅⾯发挥着关键作⽤。这种咨询⻆⾊通常表明较低级别的类似⽅法，其中 OSPO 及其成员或⼤使可以充当内部顾问，帮助开发⼈员和团队更好地理解、交互、使⽤和规划开源技术。

> 在评估可行的开源技术和帮助组织的 CTO和⼯程副总裁制定技术路线图⽅⾯发挥着关键作⽤。⼀个 OSPO，其成员可以充当内部顾问，以帮助开发⼈员和团队更好地理解、交互、使⽤和规划开源技术

## 软件公司

由于这些公司⽣产的产品与开源运动的核⼼相同，因此软件公司的OSPO往往具有略微不同的特征。在这些公司中，绝⼤多数开发⼈员通常都很好地理解和使⽤开源。这种原型更有可能孵化或参与⼤型项⽬，更有可能有专⻔从事开源⼯作的专⻔开发⼈员。例如，软件和技术公司主导着Linux 项⽬的核⼼开发团队。⼦原型是严重依赖 OSS 并且必须设计以满⾜ OSS 社区需求的技术公司。英特尔和⾼通等半导体公司符合这⼀描述。

> 帮助公司及其技术团队优先考虑OSS的使⽤，并使开源成为任何技术计划的默认⾸选。OSPO与CTO和公司战略家密切合作以映射开源项⽬和能力

随着我们对OSPO及其⽀持的组织的活动进行更多研究，此原型列表将不断发展。通过访谈和案例研究对OSPO的形成、组织和利⽤进行详细检查同样具有指导意义。

# 6. 总结：OSPO的未来

随着世界从专有软件转向开源软件，OSPO的作⽤将变得越来越重要。

在我们对OSPO领导者的采访中，我们看到了OSPO的作⽤、OSPO 的预算以及致力于在组织中推⼴开源的员⼯的普遍扩展。显然，开源已经从构建技术产品和基础设施的⽅法和思维⽅式上升为吸引顶尖⼈才和实现业务⽬标的⼿段。这与社会的数字化转型平行。

在⼀个软件吞噬⼀切的世界里，⼤⼤⼩⼩的组织都⾮常喜欢OSS，开源专业知识成为创造伟⼤产品和产品体验不可或缺的⼀部分，⽆论是在产品层（媒体、通信）还是在基础设施中⽀持产品。 OSPO正在成⻓以填补这些新的更⼤的鞋⼦，充当内部咨询、卓越中⼼以及值得信赖的顾问和导师。这样的成⻓并⾮没有成⻓的阵痛。需求开源和OSPO服务似乎供不应求，更成熟的 OSPO正在开发扩展流程和能力，以服务于这些组织内更⼴泛的⽤⼾群。

此外，OSPO不仅仅适⽤于科技行业。我们看到在学术界和政府内部建⽴了OSPO，以帮助进行软件采购和创新。

也就是说，我们与 OSPO 领导者的对话和调查回复表明，如果有的话，组织正在计划扩⼤ OSPO 预算和授权，以反映开源的增⻓。 OSPO 将拥有更多资源来⾃动化 OSPO ⼿动任务（在合规性或尽职调查等领域）和更⼤的 OSPO 团队，以满⾜在开源⽅⾯花费更多时间的开发⼈员的需求。对成功的 OSPO 的期望将从教育开发⼈员或编组代码贡献转变为增加有意义的战略价值并推动更⾼级别的开源战略、创新和开发⼈员效率。

# 7. OSPO的清单

## 阶段1

- 定义程序品牌（例如，OSPO、开源倡议、开源运营负责人）
- 管理法律风险和许可，创建新的程序和文档以确保员工根据其许可条款使用OSS，并且组织的OSS消费不会使其面临法律风险
- 创建教育计划以帮助开发人员决定何时使用OSS来创建新产品或服务
- 设置特定的软件库存流程以创建组织范围的软件物料清单 (SBOM)
- 总体而言，认识到OSS的价值以及对合规性、教育和SBOM的需求

## 阶段2

- 列出与OSS项目交互的最佳实践，例如如何请求功能、提交错误报告和贡献基本代码
- 向员工和管理人员传达贡献而不仅仅是使用 OSS 的重要性（包括倡导和推动活动赞助、预订项目负责人和维护人员作为公共编码论坛的发言人或小组成员，以及确保组织资源用于关键任务OSS项目）
- 激励开发人员从事对其运营至关重要的 OSS 项目，使开发人员成为高度活跃的贡献者或主要维护者

## 阶段3

- 发起和主持，或作为OSS项目的主要发起人
- 创建和启动开源项目以在开源社区中建立广泛的信誉
- 为项目指定一名或多名全职员工，并承担培育项目社区并确保其健康的责任
- 开发内部流程、剧本、清单、工具和其他机制来审查、组织和运营开源项目，并准备和指导他们的领导者

## 阶段4

- 成为技术决策的战略合作伙伴，帮助指导选择和塑造对项目的长期承诺
- 建议 CTO 和技术领导采用或从组织的技术堆栈中删除哪些开源技术
- 带头对什么是可接受的 OSS 项目进行基准测试
- 帮助组织理解和驾驭项目政治

[报告下载](https://8112310.fs1.hubspotusercontent-na1.net/hubfs/8112310/LF%20Research/Evolution%20of%20the%20Open%20Source%20Program%20Office%20-%20Report.pdf)

![输入图片说明](../../%E5%BC%80%E6%94%BE%E5%8E%9F%E5%AD%90%E6%A0%A1%E6%BA%90%E8%A1%8C%E8%AF%BE%E7%A8%8B%E4%BD%93%E7%B3%BB/Diagrams/by-nd2.png)

Copyright © 2022 TODO Group

