# 执行概要

这份报告涵盖了与欧洲开放源码相关的广泛主题、话题和关注。在本节中，我们将描述从我们的发现中得出的一些最令人信服的结论。我们希望这些由我们研究数据支持的结论，将为个人、组织和政府提供切实的建议，使他们能够更好地释放开源软件不断增长的价值。

我们撰写这份报告的目的是更好地了解从欧洲开始的跨区域开源动态。Linux基金会最近的研究发现，欧洲社区积极参与了研究过程。在此基础上，我们进行了一项调查和几次访谈，以探讨新兴主题。

![输入图片说明](https://foruda.gitee.com/images/1666680626072260856/236ffe74_8894319.png "World_of_Open_Source_Europe_Inforgraphic_thumbnail-1.png")
 _[资料来源](https://www.linuxfoundation.org/hubfs/World_of_Open_Source_Europe_Inforgraphic_thumbnail-1.png)_ 

## 开源是一种在欧洲合作创造价值和创新的机制

“开源”一词最早出现在1998年，用来描述以下一种工程方法，这种方法在过去十年中一直在发展:公开共享源代码，以实现一种更加开放和协作的软件开发方法。这个在上世纪90年代胡须黑客中开始的文化现象，已经转变为具有无可争辩的商业价值和软件行业的重要组成部分。

它已成为创新和通过行业合作创造共享价值的前沿主导力量。那些对我们的调查和采访做出回应的人几乎一致认为，开源对他们所在行业的现状和未来都是有价值的。也就是说，尽管信息技术行业正乘着开源创新和合作的浪潮，但其他行业却落在了后面，教育和公共部门需要进一步的投资。

我们注意到一个新兴的主题，欧洲人与开源有一种“浪漫的”关系，而北美人则有更多的商业关系。重新定位开源，在不失去社区意识的前提下释放更多商业价值，将有利于欧洲经济。

## 使用和贡献之间的不平衡对开源的可持续性提出了挑战

我们的研究为开源描绘了一幅非常积极的画面，从消费(将开放源代码纳入产品)和贡献(帮助维护开放源代码，主要通过成为开发团队的一员)中获得的衍生价值和活动的增加。然而，当我们分析细节时，一个明显的差距出现了。

在贡献方面，很大一部分受访者表示，他们所在的机构缺乏明确的政策，或他们根本不知道政策是什么。相比之下，很少有人在使用开源时看到同样的挑战。这种差距在一些部门(电信、公共、金融和保险部门)进一步扩大。

开源正遭受着日益增长的可持续性挑战;简单地说，企业倾向于“索取”多于“给予”。这种破坏性的影响在最近高调的安全事件中表现得最为明显，其根本原因是缺乏开源维护。然而，不太明显的影响是社区内日益增长的不安。

## 清晰的领导能带来红利，微观和企业规模的组织会起到带头作用

从开源中释放价值的途径远不止于简单地创建正确的策略。对于开源领域的领导者来说，它已经深深嵌入到他们的组织文化中。要实现这一目标，需要清晰可见的领导力。

我们的研究表明，通过OSPO(或者仅仅是可见的领导者)的结构化领导方法的组织，其员工受到鼓励并被授权为开源做出贡献。此外，这些组织从开源中获得的价值要大得多。

值得注意的是，在规模的两个极端(小于10或>万名员工)的组织有一个OSPO或可见的领导者。中型企业往往两者都缺乏。这些组织显然有可能追随非常小或非常大的组织的脚步，创建一个开源的领导结构，授权和支持他们的员工。

## 公共部门未能充分利用开源

我们观察了不同行业调查数据的差异;然而，公共部门的差异是最值得注意的一些。

在公共部门内，我们越来越多地看到欧洲各国和国际政府机构正式规定开源消费。此外，公共部门产生的许多代码现在都是公开共享的，这主要是出于透明度的原因。

然而，尽管有消费政策和越来越多的公共部门建立的项目，我们发现这个部门在我们的研究的许多方面都是一个例外。内部资源活动有限，这表明公共部门组织之间缺乏合作，缺乏明确的贡献政策，这可能表明对开源价值的认识过于狭隘，它只是一种工作透明度的机制，而不是协作和集体价值创造的机制。

由于公共部门从开源中获得了如此多的好处——这一主题得到了我们调查对象的坚定响应——因此需要做很多工作来创造一种文化转变。那些仅仅要求消费和“代码必须共享”的政策错过了开源所能提供的很多价值。

## 开源是培养数字主权的非政治性关键

从欧洲委员会到各国政府，数字主权国家在数字世界中独立行动的能力在整个欧洲的政治议程上都占有重要地位。无论是我们所依赖的产品、服务和基础设施的形式，还是数字创意和创新所需的技能、能力和经验的成熟，北美都在推动和拥有我们的数字世界。地缘政治事件凸显了这种依赖的风险。显然有必要建立和维持一种机制，使欧洲和欧洲国家能够在数字世界规划自己的道路。

> “开源的存在和运作超越了政治，它为所有人带来了包容性的价值。”

我们的调查答复强化了这样一个概念:开源是创新、合作、集体价值创造的强大机制，并最终将“数字公地”的愿景变为现实。有一种坚定的信念认为，行业标准和互操作性从开源中获益最多，应该进一步投资于开源替代技术垄断。关键的是，开源的存在和运作超越了政治，它为所有人带来了包容性的价值;培育人人都能使用的数字产品和服务;确保自由创新和合作的空间;为技能和能力的发展创造了丰富的环境。

# 结论

当开始一项研究项目时，你正在进行一场通往未知的旅程。虽然你可以根据之前的研究和你的直觉来指导，但你不可能知道你的研究可能会发现什么，你的旅程可能会在哪里结束。有了这项工作，我们的旅程是富有成效的，一路上我们有了许多发现。希望在阅读这篇报道时，你也能享受这段旅程。

然而，我们希望这份报告不仅仅是一个故事。我们面前有明显的机会，也有切实的问题需要解决。以下是你可以采取的一些行动，使开源对你自己、你的组织、欧洲和全球社区更容易获得、可持续和有价值:

## 通过明确的政策解决消费和贡献之间的不平衡

开源的可持续性挑战正日益被整个行业所认识到。我们的研究揭示了消费和贡献之间的不平衡，政策是问题的核心。在一些部门(电信、公共部门、金融和保险)，这一差距更大，应该以更紧迫的方式加以解决。

## 通过领导创造清晰

我们发现，只要有明确的领导(通过OSPO或个人)存在，人们就会感到更有权力。这是在上一点的基础上自然形成的，领导力和明确的政策是齐头并进的。鼓励公共部门对开源的投资。

尽管公众可以看到的消费政策和越来越多的公共部门建立的项目，我们发现这个部门在我们研究的许多方面都是一个局外人。作为公民，我们所有人都对公共部门机构如何花我们的钱有既得利益。

## 公共部门非常需要开源领导

拥抱开源，将其作为一种无关政治的协作和共享价值创造的机制。开源技术的本质和工作方式意味着它打破了界限。因为它创造的价值是为所有人设计的，它是一个固有的中立和安全的合作空间。利用这些特性为您带来好处，促进非商业参与和专家之间的工作关系，无论他们来自哪里。
 
_报告来源：Linux Foundation Research_ 
_[报告下载地址](https://8112310.fs1.hubspotusercontent-na1.net/hubfs/8112310/Research%20Reports/World_of_Open_Source_Europe_091322.pdf)_ 
_Copyright © 2022 The Linux Foundation | September 2022_ 

![CC4.0](https://foruda.gitee.com/images/1666681568689981505/0ecfaebc_8894319.png "by-nd (2).png")